package com.deforge.accountms.util.kafka.serializer;


import java.util.Map;

import com.deforge.accountms.model.response.CurrencyResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.kafka.common.serialization.Deserializer;



public class CurrencyResponseDeserializer implements Deserializer<CurrencyResponse> {
    private final ObjectMapper objectMapper = new ObjectMapper();
    public static CurrencyResponse UPDATED_RESPONSE = new CurrencyResponse();


    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }

    @SneakyThrows
    @Override
    public CurrencyResponse deserialize(String topic, byte[] data) {
        if (data == null) {
            return new CurrencyResponse();
        }
        CurrencyResponse response = objectMapper.readValue(data, CurrencyResponse.class);
        UPDATED_RESPONSE = response;
        System.out.println(response.toString());
        return response;
    }

    @Override
    public void close() {

    }

}
