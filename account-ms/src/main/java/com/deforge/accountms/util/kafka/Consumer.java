package com.deforge.accountms.util.kafka;

import com.deforge.accountms.model.response.CurrencyResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class Consumer {

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    public static CurrencyResponse CURRENCY_DATA = new CurrencyResponse();

    @KafkaListener(topics = "CURRENCY_TOPIC",
            groupId = "account_group_id",
            clientIdPrefix = "consumer-",
            containerFactory = "kafkaListenerContainerFactory",
            concurrency = "3"
    )
    public CurrencyResponse consumeMessage(@Payload CurrencyResponse currencyResponse) {
        logger.info("CONSUMED DATA: {}", currencyResponse);
        CURRENCY_DATA = currencyResponse;
        return currencyResponse;
    }

}
