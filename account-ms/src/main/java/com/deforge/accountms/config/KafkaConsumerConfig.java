package com.deforge.accountms.config;

import com.deforge.accountms.model.response.CurrencyResponse;
import com.deforge.accountms.util.kafka.serializer.CurrencyResponseDeserializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

    @Bean
    Map<String, Object> config() {
        Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, "account_group_id");
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CurrencyResponseDeserializer.class);
        return config;
    }

    @Bean
    public ConsumerFactory<String, CurrencyResponse> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(config());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CurrencyResponse> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, CurrencyResponse> factory
                = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    @Bean
    public ConcurrentMessageListenerContainer<String, CurrencyResponse> messageListenerContainer() {
        ConcurrentMessageListenerContainer<String, CurrencyResponse> container =
                kafkaListenerContainerFactory().createContainer("CURRENCY_TOPIC");
        container.setupMessageListener((MessageListener<String, CurrencyResponse>) record -> {
            System.out.println("Received Message: " + record.value());
        });
        return container;
    }

}
