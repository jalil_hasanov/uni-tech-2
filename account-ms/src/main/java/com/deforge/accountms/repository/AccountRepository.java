package com.deforge.accountms.repository;

import com.deforge.accountms.model.entity.AccountEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    List<AccountEntity> findAccountEntitiesByPin(String pin);
    Boolean existsAccountEntitiesByPinAndAccountNumber(String pin, String accountNumber);

    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<AccountEntity> findAccountEntityByAccountNumber(String accountNumber);
}
