package com.deforge.accountms.controller;

import com.deforge.accountms.model.dto.request.TransferRequestDto;
import com.deforge.accountms.model.dto.response.TransferResponseDto;
import com.deforge.accountms.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {
    private final TransferService transferService;

    @PostMapping
    public ResponseEntity<TransferResponseDto> transfer(
            @RequestHeader String pin,
            @RequestBody TransferRequestDto request
    ) {
        return ResponseEntity.ok(transferService.transfer(pin, request));
    }
}
