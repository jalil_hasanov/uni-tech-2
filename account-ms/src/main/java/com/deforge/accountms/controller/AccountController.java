package com.deforge.accountms.controller;

import com.deforge.accountms.model.dto.request.AccountRegistrationRequest;
import com.deforge.accountms.model.dto.response.AccountResponseDto;
import com.deforge.accountms.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService service;

    @GetMapping("/list")
    public ResponseEntity<List<AccountResponseDto>> accountEntity(
            @RequestHeader String pin
    ){
        return ResponseEntity.ok(service.getAllAccountsByPin(pin));
    }

    @PostMapping("/create")
    public ResponseEntity<AccountResponseDto> createAccount(
            @RequestHeader String pin,
            @RequestBody AccountRegistrationRequest request
    ){
        return ResponseEntity.ok(service.createAccount(pin, request));
    }
}
