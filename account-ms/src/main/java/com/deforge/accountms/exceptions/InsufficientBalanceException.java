package com.deforge.accountms.exceptions;

public class InsufficientBalanceException extends RuntimeException {

    public InsufficientBalanceException(String accountNumber) {
        super("THERE IS NOT ENOUGH BALANCE IN YOUR ACCOUNT " + accountNumber);
    }
}
