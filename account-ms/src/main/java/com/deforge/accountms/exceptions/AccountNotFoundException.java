package com.deforge.accountms.exceptions;

public class AccountNotFoundException extends RuntimeException{
    public AccountNotFoundException(String accountNumber) {
        super("Account: "+accountNumber+" not found!!!");
    }
}
