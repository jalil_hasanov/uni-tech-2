package com.deforge.accountms.exceptions;

public class UnacceptableSenderAccount extends RuntimeException{

    public UnacceptableSenderAccount(String pin, String senderAccountNumber) {
        super("Your pin(" + pin + ") is not related with this account(" + senderAccountNumber + ")");
    }


}
