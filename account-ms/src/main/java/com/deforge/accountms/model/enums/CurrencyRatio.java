package com.deforge.accountms.model.enums;

public enum CurrencyRatio {
    AZN_TO_USD, USD_TO_AZN, AZN_TO_EUR, EUR_TO_AZN, AZN_TO_TL, TL_TO_AZN,
    USD_TO_EUR, EUR_TO_USD, USD_TO_TL, TL_TO_USD,
    EUR_TO_TL, TL_TO_EUR;

}
