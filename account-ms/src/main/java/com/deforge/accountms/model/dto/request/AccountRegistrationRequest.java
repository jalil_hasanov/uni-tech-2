package com.deforge.accountms.model.dto.request;

import com.deforge.accountms.model.enums.CurrencyEnum;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record AccountRegistrationRequest(
        BigDecimal balance,
        String accountNumber ,
        CurrencyEnum currency
) {

}
