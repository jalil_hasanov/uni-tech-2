package com.deforge.accountms.model.response;

import com.deforge.accountms.model.enums.CurrencyRatio;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CurrencyResponse implements Serializable {
        @Builder.Default
        Map<CurrencyRatio, Double> currencyMap = new HashMap<>();
}
