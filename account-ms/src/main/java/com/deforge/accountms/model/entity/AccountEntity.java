package com.deforge.accountms.model.entity;

import com.deforge.accountms.model.enums.CurrencyEnum;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "account")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_seq_generator")
    @SequenceGenerator(
            name = "account_seq_generator",
            sequenceName = "account_seq",
            allocationSize = 1
    )
    @Column(name = "account_id")
    Long accountId;
    @Column(name = "pin", nullable = false)
    String pin;
    String accountNumber;
    BigDecimal balance;
    @Enumerated(value = EnumType.STRING)
    CurrencyEnum currency;
    @CreationTimestamp
    LocalDateTime createdAt;
    @UpdateTimestamp
    LocalDateTime updatedAt;
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        this.updatedAt = createdAt;
    }

}
