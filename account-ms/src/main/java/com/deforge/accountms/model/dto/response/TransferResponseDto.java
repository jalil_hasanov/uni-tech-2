package com.deforge.accountms.model.dto.response;

import lombok.Builder;

import java.math.BigDecimal;

@Builder
public record TransferResponseDto(
        String status,
        Double transferAmount
) {
}
