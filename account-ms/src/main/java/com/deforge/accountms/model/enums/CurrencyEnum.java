package com.deforge.accountms.model.enums;

public enum CurrencyEnum {
    AZN, EUR, TL, USD
}
