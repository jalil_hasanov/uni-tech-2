package com.deforge.accountms.model.dto.request;

import lombok.Builder;

@Builder
public record TransferRequestDto(
        String senderAccountNumber,
        String receiverAccountNumber,
        Double amount
) {
}
