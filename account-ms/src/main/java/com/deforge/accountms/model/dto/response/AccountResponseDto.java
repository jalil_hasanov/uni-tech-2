package com.deforge.accountms.model.dto.response;

import com.deforge.accountms.model.enums.CurrencyEnum;
import lombok.Builder;

import java.math.BigDecimal;

@Builder
public record AccountResponseDto(
        String pin,
        String accountNumber,
        BigDecimal balance,
        CurrencyEnum currency
) {
}
