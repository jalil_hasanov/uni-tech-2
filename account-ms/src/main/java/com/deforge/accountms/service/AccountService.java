package com.deforge.accountms.service;

import com.deforge.accountms.model.dto.request.AccountRegistrationRequest;
import com.deforge.accountms.model.dto.response.AccountResponseDto;

import java.util.List;

public interface AccountService {
    List<AccountResponseDto> getAllAccountsByPin(String pin);

    AccountResponseDto createAccount(String pin, AccountRegistrationRequest request);
}
