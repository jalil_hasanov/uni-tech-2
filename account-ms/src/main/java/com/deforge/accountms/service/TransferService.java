package com.deforge.accountms.service;

import com.deforge.accountms.model.dto.request.TransferRequestDto;
import com.deforge.accountms.model.dto.response.TransferResponseDto;

public interface TransferService {
    TransferResponseDto transfer(String pin, TransferRequestDto request);
}
