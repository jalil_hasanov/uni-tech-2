package com.deforge.accountms.service.impl;

import com.deforge.accountms.model.dto.request.AccountRegistrationRequest;
import com.deforge.accountms.model.dto.response.AccountResponseDto;
import com.deforge.accountms.model.entity.AccountEntity;
import com.deforge.accountms.repository.AccountRepository;
import com.deforge.accountms.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    @Override
    public List<AccountResponseDto> getAllAccountsByPin(String pin) {
        return accountRepository.findAccountEntitiesByPin(pin).stream().map(
                acc -> AccountResponseDto.builder()
                        .accountNumber(acc.getAccountNumber())
                        .balance(acc.getBalance())
                        .pin(acc.getPin())
                        .currency(acc.getCurrency())
                        .build()
        ).toList();
    }

    @Override
    public AccountResponseDto createAccount(String pin, AccountRegistrationRequest request) {
        AccountEntity entity = AccountEntity.builder()
                .balance(request.balance())
                .currency(request.currency())
                .accountNumber(request.accountNumber())
                .pin(pin)
                .build();
        AccountEntity acc = accountRepository.save(entity);
        return AccountResponseDto.builder()
                .accountNumber(acc.getAccountNumber())
                .balance(acc.getBalance())
                .pin(acc.getPin())
                .currency(acc.getCurrency())
                .build();
    }
}
