package com.deforge.accountms.service.impl;

import com.deforge.accountms.model.enums.CurrencyRatio;
import com.deforge.accountms.exceptions.AccountNotFoundException;
import com.deforge.accountms.exceptions.InsufficientBalanceException;
import com.deforge.accountms.exceptions.UnacceptableSenderAccount;
import com.deforge.accountms.model.dto.request.TransferRequestDto;
import com.deforge.accountms.model.dto.response.TransferResponseDto;
import com.deforge.accountms.model.entity.AccountEntity;
import com.deforge.accountms.model.enums.CurrencyEnum;
import com.deforge.accountms.repository.AccountRepository;
import com.deforge.accountms.service.TransferService;
import jakarta.persistence.OptimisticLockException;
import jakarta.persistence.PessimisticLockException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static com.deforge.accountms.util.kafka.serializer.CurrencyResponseDeserializer.UPDATED_RESPONSE;


@Service
@RequiredArgsConstructor
public class TransferServiceImpl implements TransferService {

    private final AccountRepository accountRepository;


    @Override
    @Transactional(rollbackOn = {
            InsufficientBalanceException.class,
            UnacceptableSenderAccount.class,
            AccountNotFoundException.class,
            PessimisticLockException.class
    })
    public TransferResponseDto transfer(String pin, TransferRequestDto request) {
        if (accountRepository
                .existsAccountEntitiesByPinAndAccountNumber(pin, request.senderAccountNumber()) &&
                !request.senderAccountNumber().equals(request.receiverAccountNumber())
        ) {
            AccountEntity senderAccount =
                    accountRepository
                            .findAccountEntityByAccountNumber(request.senderAccountNumber())
                            .orElseThrow(() -> new AccountNotFoundException(request.senderAccountNumber()));
            AccountEntity receiverAccount =
                    accountRepository
                            .findAccountEntityByAccountNumber(request.receiverAccountNumber())
                            .orElseThrow(() -> new AccountNotFoundException(request.receiverAccountNumber()));
            if (request.amount() > senderAccount.getBalance().doubleValue()) {
                throw new InsufficientBalanceException(request.senderAccountNumber());
            }
            BigDecimal amountToSend = amountToSendByCurrency(
                    senderAccount.getCurrency(), receiverAccount.getCurrency(), request.amount()
            );
            senderAccount.setBalance(senderAccount.getBalance().subtract(BigDecimal.valueOf(request.amount())));
            receiverAccount.setBalance(receiverAccount.getBalance().add(amountToSend));
            return TransferResponseDto.builder()
                    .transferAmount(request.amount())
                    .status("OK")
                    .build();
        } else {
            throw new UnacceptableSenderAccount(pin, request.senderAccountNumber());
        }

    }

    private BigDecimal amountToSendByCurrency(
            CurrencyEnum currencyOfSender,
            CurrencyEnum currencyOfReceiver,
            Double amount) {
        if (currencyOfSender.equals(currencyOfReceiver)){
            return BigDecimal.valueOf(amount);
        }
        CurrencyRatio currencyRatio = generateCurrencyRatio(currencyOfSender, currencyOfReceiver);
        Double ratio = UPDATED_RESPONSE.getCurrencyMap().get(currencyRatio);
        return BigDecimal.valueOf(ratio).multiply(BigDecimal.valueOf(amount));
    }

    private CurrencyRatio generateCurrencyRatio(
            CurrencyEnum currencyOfSender,
            CurrencyEnum currencyOfReceiver
    ) {
        return CurrencyRatio.valueOf(
                currencyOfSender.name().concat("_TO_").concat(currencyOfReceiver.name()));
    }
}
