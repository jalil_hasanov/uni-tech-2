package com.deforge.currencyms.util;

import com.deforge.currencyms.model.enums.CurrencyRatio;
import com.deforge.currencyms.model.enums.CurrencyResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.deforge.currencyms.model.enums.CurrencyRatio.*;

@Component
@RequiredArgsConstructor
public class CurrencyUtil {

    private final KafkaTemplate<String, CurrencyResponse> kafkaTemplate;

    private static int count = 0;

    public static Map<CurrencyRatio, Double> RATIOS = new HashMap<>();

    public Map<CurrencyRatio, Double> getCurrency() {
        RATIOS.put(AZN_TO_USD, format(1 / 1.7));
        RATIOS.put(USD_TO_AZN, format(1.7));
        RATIOS.put(AZN_TO_TL, format(8D));
        RATIOS.put(TL_TO_AZN, format(1D / 8D));
        RATIOS.put(AZN_TO_EUR, format(0.5D));
        RATIOS.put(EUR_TO_AZN, format(2D));
        RATIOS.put(USD_TO_EUR, format(0.91D));
        RATIOS.put(EUR_TO_USD, format(1D / 0.91D));
        RATIOS.put(USD_TO_TL, format(27.77D));
        RATIOS.put(TL_TO_USD, format(1D / 27.77D));
        RATIOS.put(EUR_TO_TL, format(32.6D));
        RATIOS.put(TL_TO_EUR, format(1D / 32.6D));
        return RATIOS;
    }

    public Map<CurrencyRatio, Double> getUpdatedCurrency() {
        RATIOS.put(AZN_TO_USD, format(1 / 1.8));
        RATIOS.put(USD_TO_AZN, format(1.8));
        RATIOS.put(AZN_TO_TL, format(8.5D));
        RATIOS.put(TL_TO_AZN, format(1D / 8.5D));
        RATIOS.put(AZN_TO_EUR, format(0.6D));
        RATIOS.put(EUR_TO_AZN, format(2.1D));
        RATIOS.put(USD_TO_EUR, format(0.92D));
        RATIOS.put(EUR_TO_USD, format(1D / 0.92D));
        RATIOS.put(USD_TO_TL, format(27.8D));
        RATIOS.put(TL_TO_USD, format(1D / 27.8D));
        RATIOS.put(EUR_TO_TL, format(32.3D));
        RATIOS.put(TL_TO_EUR, format(1D / 32.3D));
        return RATIOS;
    }

    @Scheduled(fixedRate = 60000)
    public void renewCachePeriodically() {
        RATIOS = new HashMap<>();
        if (count%2==0){
            RATIOS = getUpdatedCurrency();
            count++;
            System.out.println("MESSAGE SENT...");
            kafkaTemplate.send("CURRENCY_TOPIC",CurrencyResponse.builder()
                            .currencyMap(RATIOS).build());
        }else {
            RATIOS = getCurrency();
            System.out.println("MESSAGE SENT...");
            kafkaTemplate.send("CURRENCY_TOPIC",CurrencyResponse.builder()
                    .currencyMap(RATIOS).build());
            count++;
        }
    }

    private static Double format(Double d) {
        return Double.valueOf(String.format("%.3f", d));
    }
}
