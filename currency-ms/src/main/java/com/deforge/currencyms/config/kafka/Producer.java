package com.deforge.currencyms.config.kafka;

import com.deforge.currencyms.model.enums.CurrencyResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Producer {
    private static String TOPIC = "CURRENCY_TOPIC";
    private final KafkaTemplate<String, CurrencyResponse> kafkaTemplate;

    public void sendMessage(CurrencyResponse message){
        this.kafkaTemplate.send(TOPIC, message);
    }
}
