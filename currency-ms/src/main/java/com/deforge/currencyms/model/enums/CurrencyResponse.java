package com.deforge.currencyms.model.enums;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.Map;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CurrencyResponse implements Serializable {
        Map<CurrencyRatio, Double> currencyMap;
}
