package com.deforge.currencyms.controller;

import com.deforge.currencyms.model.enums.CurrencyResponse;
import com.deforge.currencyms.util.CurrencyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/currency")
public class CurrencyController {

    @Autowired
    private CurrencyUtil currencyUtil;
    @Autowired


    @GetMapping("/ratio")
    public ResponseEntity<CurrencyResponse> getCurrency() {
        CurrencyResponse currencyResponse =new CurrencyResponse(currencyUtil.RATIOS);
        return ResponseEntity.ok(currencyResponse);
    }



}
