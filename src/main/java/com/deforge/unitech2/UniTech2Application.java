package com.deforge.unitech2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniTech2Application {

    public static void main(String[] args) {
        SpringApplication.run(UniTech2Application.class, args);
    }

}
