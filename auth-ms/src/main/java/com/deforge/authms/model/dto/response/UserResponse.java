package com.deforge.authms.model.dto.response;

import lombok.Builder;

@Builder
public record UserResponse(
        String fullName, String email
) {
}
