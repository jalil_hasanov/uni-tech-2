package com.deforge.authms.model.dto.request;

import lombok.Builder;

@Builder
public record AuthenticateRequest(
        String pin, String password
) {
}
