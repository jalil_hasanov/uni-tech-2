package com.deforge.authms.model.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_credential")
public class UserCredentialEntity {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_credentials_seq_gen")
    @SequenceGenerator(
            name = "user_credentials_seq_gen",
            sequenceName = "user_credentials_seq",
            allocationSize = 1
    )
    Long userId;
    String fullName;
    @Column(name = "mail", unique = true)
    String mail;
    @Column(name = "pin", unique = true)
    String pin;
    String password;

}
