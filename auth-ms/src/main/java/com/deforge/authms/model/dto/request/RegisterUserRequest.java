package com.deforge.authms.model.dto.request;

public record RegisterUserRequest(
        String fullName,
        String mail,
        String pin,
        String password
) {
}
