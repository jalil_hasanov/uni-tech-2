package com.deforge.authms.exception;

public class PinNotFoundException extends Exception {
    public PinNotFoundException() {
        super("Pin and/or password are incorrect!!!");
    }
}
