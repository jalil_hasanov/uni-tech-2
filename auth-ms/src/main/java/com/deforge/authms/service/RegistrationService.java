package com.deforge.authms.service;

import com.deforge.authms.model.dto.request.RegisterUserRequest;
import com.deforge.authms.model.dto.response.UserResponse;

public interface RegistrationService {
    UserResponse register(RegisterUserRequest request);
}
