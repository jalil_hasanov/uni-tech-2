package com.deforge.authms.service.impl;

import com.deforge.authms.exception.PinNotFoundException;
import com.deforge.authms.model.dto.request.AuthenticateRequest;
import com.deforge.authms.model.entity.UserCredentialEntity;
import com.deforge.authms.model.security.UserDetailsImpl;
import com.deforge.authms.repository.RegistrationRepository;
import com.deforge.authms.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService, UserDetailsService {

    private final RegistrationRepository repository;

    @Override
    public UserDetails loadUserByUsername(String pin)
            throws UsernameNotFoundException {
        UserCredentialEntity entity = repository.findByPin(pin)
                .orElseThrow(() -> new UsernameNotFoundException("PIN or PASSWORD were INCORRECT!!!"));
        return UserDetailsImpl.builder()
                .user(entity)
                .build();
    }

    @Override
    public void authenticated(AuthenticateRequest request)
            throws PinNotFoundException {
        UserCredentialEntity userDetails = repository.findByPin(request.pin())
                .orElseThrow(() -> new UsernameNotFoundException("PIN or PASSWORD were INCORRECT!!!"));
        if (!new BCryptPasswordEncoder().matches(request.password(), userDetails.getPassword())) {
            throw new PinNotFoundException();
        }
    }


}
