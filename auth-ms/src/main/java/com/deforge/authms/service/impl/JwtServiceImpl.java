package com.deforge.authms.service.impl;

import com.deforge.authms.exception.PinNotFoundException;
import com.deforge.authms.model.dto.request.AuthenticateRequest;
import com.deforge.authms.model.entity.UserCredentialEntity;
import com.deforge.authms.repository.RegistrationRepository;
import com.deforge.authms.service.AuthService;
import com.deforge.authms.service.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService {
    private final AuthService authService;

    private static final String KEY = "aM30TI6poM2VH4PoQAztHCi0ErAE3vsKEIA6ej69TBA=";
    @Override
    public String generateToken(AuthenticateRequest request, Map<String, Object> claims)
            throws PinNotFoundException {
        String pin = request.pin();
        authService.authenticated(request);
        return Jwts.builder()
                .setSubject(pin)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 30))
                .signWith(getKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    private Key getKey() {
        byte[] keyBytes = Decoders.BASE64.decode(KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    @Override
    public String extractPinFromToken(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    @Override
    public Date extractExpirationDate(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    @Override
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpirationDate(token).before(new Date());
    }

    @Override
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String pin = extractPinFromToken(token);
        return (pin.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    @Override
    public Boolean validateToken(String token) {
        String pin =extractPinFromToken(token);
        UserDetails userDetails = authService.loadUserByUsername(pin);
        return (pin.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }


}
