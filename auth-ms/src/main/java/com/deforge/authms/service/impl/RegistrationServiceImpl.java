package com.deforge.authms.service.impl;

import com.deforge.authms.model.entity.UserCredentialEntity;
import com.deforge.authms.repository.RegistrationRepository;
import com.deforge.authms.service.RegistrationService;
import com.deforge.authms.model.dto.request.RegisterUserRequest;
import com.deforge.authms.model.dto.response.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {
    private final RegistrationRepository userRegistrationRepo;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserResponse register(RegisterUserRequest request) {
        String encodedPassword = passwordEncoder.encode(request.password());
        UserCredentialEntity entity = UserCredentialEntity.builder()
                .pin(request.pin())
                .mail(request.mail())
                .fullName(request.fullName())
                .password(encodedPassword)
                .build();
        UserCredentialEntity repoResponse = userRegistrationRepo.save(entity);
        return UserResponse.builder()
                .email(repoResponse.getMail())
                .fullName(repoResponse.getFullName())
                .build();
    }
}
