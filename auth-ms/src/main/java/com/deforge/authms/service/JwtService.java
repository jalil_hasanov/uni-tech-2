package com.deforge.authms.service;

import com.deforge.authms.exception.PinNotFoundException;
import com.deforge.authms.model.dto.request.AuthenticateRequest;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.Map;
import java.util.function.Function;

public interface JwtService {
    String generateToken(AuthenticateRequest request, Map<String, Object> claims) throws PinNotFoundException;

    String extractPinFromToken(String token);

    Date extractExpirationDate(String token);

    <T> T extractClaim(String token, Function<Claims, T> claimsResolver);

    Boolean validateToken(String token, UserDetails userDetails);

    Boolean validateToken(String token);

}
