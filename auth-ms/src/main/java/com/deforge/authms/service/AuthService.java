package com.deforge.authms.service;

import com.deforge.authms.exception.PinNotFoundException;
import com.deforge.authms.model.dto.request.AuthenticateRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

public interface AuthService {
    UserDetails loadUserByUsername(String pin);

    void authenticated(AuthenticateRequest request) throws PinNotFoundException;
}
