package com.deforge.authms.repository;

import com.deforge.authms.model.entity.UserCredentialEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RegistrationRepository extends JpaRepository<UserCredentialEntity, Long> {

    Optional<UserCredentialEntity> findByPin(String pin);

}
