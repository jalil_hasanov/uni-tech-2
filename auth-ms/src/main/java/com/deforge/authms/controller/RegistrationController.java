package com.deforge.authms.controller;

import com.deforge.authms.service.RegistrationService;
import com.deforge.authms.model.dto.request.RegisterUserRequest;
import com.deforge.authms.model.dto.response.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
@RequiredArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;

    @PostMapping
    public ResponseEntity<UserResponse> register(@RequestBody RegisterUserRequest request){
        return ResponseEntity.ok(registrationService.register(request));
    }
}
