package com.deforge.authms.controller;

import com.deforge.authms.model.dto.request.AuthenticateRequest;
import com.deforge.authms.service.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/jwt")
@RequiredArgsConstructor
public class JwtController {

    private final JwtService service;

    @SneakyThrows
    @PostMapping("/authenticate")
    public ResponseEntity<String> authenticate(
            @RequestHeader Map<String, Object> headers,
            @RequestBody AuthenticateRequest request
    ) {
        return ResponseEntity.ok(service.generateToken(request, headers));
    }

    @SneakyThrows
    @GetMapping("/validate")
    public ResponseEntity<Boolean> validateToken(
            @RequestParam String token
    ) {
        return ResponseEntity.ok(service.validateToken(token));
    }
}
