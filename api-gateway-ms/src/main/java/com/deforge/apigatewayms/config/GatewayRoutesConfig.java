package com.deforge.apigatewayms.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.deforge.apigatewayms.filter.JwtAuthFilter.jwtAuthGatewayFilter;
import static com.deforge.apigatewayms.filter.JwtRequestHeaderFilter.jwtRequestHeaderGatewayFilter;

@Configuration
public class GatewayRoutesConfig {

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r
                        .path("/auth-ms/register")
                        .uri("lb://auth-ms"))
                .route(r -> r
                        .path("/auth-ms/jwt/**")
                        .uri("lb://auth-ms"))
                .route(r -> r
                        .path("/auth-ms/health")
                        .uri("lb://auth-ms"))
                .route(r -> r
                        .path("/account-ms/account/**")
                        .filters(filter ->
                                filter.filters(jwtAuthGatewayFilter, jwtRequestHeaderGatewayFilter))
                        .uri("lb://account-ms"))
                .route(r -> r
                        .path("/account-ms/transfer")
                        .filters(filter ->
                                filter.filters(jwtAuthGatewayFilter, jwtRequestHeaderGatewayFilter))
                        .uri("lb://account-ms"))
                .route(r -> r
                        .path("/currency-ms/currency/ratio")
                        .uri("lb://currency-ms"))
                .build();
    }
}
