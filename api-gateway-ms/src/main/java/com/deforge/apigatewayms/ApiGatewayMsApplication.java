package com.deforge.apigatewayms;

import com.deforge.apigatewayms.filter.JwtAuthFilter;
import com.deforge.apigatewayms.filter.JwtRequestHeaderFilter;
import com.deforge.apigatewayms.util.JwtUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
public class ApiGatewayMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayMsApplication.class, args);
    }

}
