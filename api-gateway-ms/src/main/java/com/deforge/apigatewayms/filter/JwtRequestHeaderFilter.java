package com.deforge.apigatewayms.filter;

import com.deforge.apigatewayms.util.JwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class JwtRequestHeaderFilter extends AbstractGatewayFilterFactory<JwtAuthFilter.Config> {

    private RouteValidator routeValidator;
    private JwtUtil jwtUtil;

    public final static GatewayFilter jwtRequestHeaderGatewayFilter =
            new JwtRequestHeaderFilter().apply(new JwtAuthFilter.Config());


    public JwtRequestHeaderFilter() {
        super(JwtAuthFilter.Config.class);
        routeValidator = new RouteValidator();
        jwtUtil = new JwtUtil();
    }

    @Override
    public GatewayFilter apply(JwtAuthFilter.Config config) {
        return (((exchange, chain) -> {
            if (routeValidator.isSecured.test(exchange.getRequest())) {
                if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
                    throw new RuntimeException("MISSING AUTH HEADER...");
                }

                String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
                if (StringUtils.hasText(authHeader) && authHeader.startsWith("Bearer ")) {
                    authHeader = authHeader.substring(7);
                }
                exchange.getRequest().mutate().header("pin", jwtUtil.extractClaim(authHeader, Claims::getSubject));
            }
            return chain.filter(exchange);
        }));
    }

    public static class Config {

    }
}
